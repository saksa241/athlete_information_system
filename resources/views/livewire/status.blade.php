<div>
    <select wire:model="status" name="status" class="mt-1 w-full text-center border-gray-300 rounded-md shadow-sm focus:border-primary-300 focus:ring focus:ring-primary-200 focus:ring-opacity-50 focus-within:text-primary-600">
        <option value="Active" @if (auth()->user()->status == 'Active') selected @endif>Active</option>
        <option value="Injured" @if (auth()->user()->status == 'Injured') selected @endif>Injured</option>
        <option value="Study Leave" @if (auth()->user()->status == 'Study Leave') selected @endif>Study Leave</option>
    </select>

</div>
