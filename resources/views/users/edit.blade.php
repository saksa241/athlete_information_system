<x-app-layout>
    <x-slot name="header">
        {{ __('Update Athlete') }}
    </x-slot>

    <div class="p-4 bg-white rounded-lg shadow-xs">
        <x-auth-validation-errors />

        <div class="p-5 overflow-hidden w-full rounded-lg border shadow-xs">
            <div class="overflow-x-auto w-full">
                <div class="flex items-center justify-center p-6 sm:p-12">
                    <div class="w-full">
                        <form method="POST" action="{{ route('users.update', $user) }}" enctype="multipart/form-data">
                            @method('PUT')
                            @csrf
                            <div class="w-full">
                                <div class="mb-3">
                                    <x-label for="name" :value="__('Name')"/>
                                    <x-input name="name"
                                            type="text"
                                            class="block w-full"
                                            value="{{ $user->name }}"/>
                                </div>
                                <div class="my-3">
                                    <x-label for="email" :value="__('Email')"/>
                                    <x-input name="email"
                                            type="email"
                                            class="block w-full"
                                            value="{{$user->email }}"/>
                                </div>

                                <div class="my-3 grid grid-cols-2 gap-4 ">

                                   <div class="my-3">
                                        <x-label for="course_id" :value="__('Course')"/>
                                        <select name="course_id" class="mt-1 w-full border-gray-300 rounded-md shadow-sm focus:border-primary-300 focus:ring focus:ring-primary-200 focus:ring-opacity-50 focus-within:text-primary-600">
                                            @foreach ($courses as $course )
                                                <option value="{{ $course->id }}" @if ($user->course_id == $course->id) selected @endif>{{ $course->description }}</option>
                                            @endforeach
                                        </select>
                                   </div>

                                   <div class="my-3">
                                    <x-label for="year_level" :value="__('Year Level')"/>
                                        <select name="year_level" class="mt-1 w-full border-gray-300 rounded-md shadow-sm focus:border-primary-300 focus:ring focus:ring-primary-200 focus:ring-opacity-50 focus-within:text-primary-600">
                                            <option value="First Year" @if ($user->year_level == 'First Year') selected @endif>First Year</option>
                                            <option value="Second Year" @if ($user->year_level == 'Second Year') selected @endif>Second Year</option>
                                            <option value="Third Year" @if ($user->year_level == 'Third Year') selected @endif>Third Year</option>
                                            <option value="Fourth Year" @if ($user->year_level == 'Fourth Year') selected @endif>Fourth Year</option>
                                        </select>
                                   </div>
                                </div>

                                <div class="grid grid-cols-2 gap-4">

                                    <div class="my-3">
                                         <x-label for="age" :value="__('Age')"/>
                                         <x-input name="age"
                                                 type="number"
                                                 class="block w-full"
                                                 value="{{ $user->age }}"/>
                                    </div>

                                    <div class="my-3">
                                     <x-label for="birthday" :value="__('Birthday')"/>
                                     <x-input name="birthday"
                                             type="date"
                                             class="block  w-full"
                                             value="{{ $user->birthday }}"/>
                                     </div>
                                 </div>

                                 <div class="my-3">
                                    <x-label for="address" :value="__('Address')"/>
                                    <x-input name="address"
                                            type="text"
                                            class="block w-full"
                                            value="{{ $user->address }}"/>
                                </div>

                                <div class="grid grid-cols-2 gap-4">
                                    <div class="my-3">
                                        <x-label for="sports" :value="__('Sports')"/>
                                        <x-input name="sports"
                                                type="text"
                                                class="block w-full"
                                                value="{{ $user->sports }}"/>
                                    </div>
                                    <div class="my-3">
                                        <x-label for="status" :value="__('Status')"/>
                                        <select name="status" class="mt-1 w-full border-gray-300 rounded-md shadow-sm focus:border-primary-300 focus:ring focus:ring-primary-200 focus:ring-opacity-50 focus-within:text-primary-600">
                                            <option value="Active" @if ($user->status == 'Active') selected @endif>Active</option>
                                            <option value="Injured" @if ($user->status == 'Injured') selected @endif>Injured</option>
                                            <option value="Study Leave" @if ($user->status == 'Study Leave') selected @endif>Study Leave</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="my-3 flex">

                                    @if ($user->avatar)
                                        <img class="h-10 w-10 rounded-full" src="{{ asset('images/profile/'.$user->avatar) }}" alt="">
                                    @endif
                                    <div class="mx-3">
                                        <x-label for="avatar" :value="__('Picture')"/>
                                    <x-input name="avatar"
                                            type="file"
                                            class="block w-full"
                                            value="{{ old('avatar') }}"/>
                                    </div>
                                </div>
                            </div>

                            <div class="mt-4">
                                <x-button class="block ">
                                    {{ __('Update Athlete') }}
                                </x-button>
                            </div>
                        </form>
                </div>
            </div>

        </div>

    </div>
</x-app-layout>
