<x-app-layout>
    <x-slot name="header">
        {{ __('Create Athlete') }}
    </x-slot>

    <div class="p-4 bg-white rounded-lg shadow-xs">
        <div class="p-5 overflow-hidden w-full rounded-lg border shadow-xs">
            <x-auth-validation-errors />
            <div class="overflow-x-auto w-full">
                <div class="flex items-center justify-center p-6 sm:p-12">
                    <div class="w-full">
                        <form method="POST" action="{{ route('users.store') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="w-full">
                                <div class="mb-3">
                                    <x-label for="name" :value="__('Name')"/>
                                    <x-input name="name"
                                            type="text"
                                            class="block w-full"
                                            value="{{ old('name') }}"/>
                                </div>
                                <div class="my-3">
                                    <x-label for="email" :value="__('Email')"/>
                                    <x-input name="email"
                                            type="email"
                                            class="block w-full"
                                            value="{{ old('email') }}"/>
                                </div>

                                <div class="my-3 grid grid-cols-2 gap-4 ">

                                   <div class="my-3">
                                        <x-label for="course_id" :value="__('Course')"/>
                                        <select name="course_id" class="mt-1 w-full border-gray-300 rounded-md shadow-sm focus:border-primary-300 focus:ring focus:ring-primary-200 focus:ring-opacity-50 focus-within:text-primary-600">
                                            <option value="" selected disabled>-- Select Course --</option>
                                            @foreach ($courses as $course )
                                                <option value="{{ $course->id }}">{{ $course->description }}</option>
                                            @endforeach
                                        </select>
                                   </div>

                                   <div class="my-3">
                                    <x-label for="year_level" :value="__('Year Level')"/>
                                        <select name="year_level" class="mt-1 w-full border-gray-300 rounded-md shadow-sm focus:border-primary-300 focus:ring focus:ring-primary-200 focus:ring-opacity-50 focus-within:text-primary-600">
                                            <option value="" selected disabled>-- Select Yearl Level --</option>
                                            <option value="First Year">First Year</option>
                                            <option value="Second Year">Second Year</option>
                                            <option value="Third Year">Third Year</option>
                                            <option value="Fourth Year">Fourth Year</option>
                                        </select>
                                   </div>

                                </div>

                                <div class="grid grid-cols-2 gap-4">

                                    <div class="my-3">
                                         <x-label for="age" :value="__('Age')"/>
                                         <x-input name="age"
                                                 type="number"
                                                 class="block w-full"
                                                 value="{{ old('age') }}"/>
                                    </div>

                                    <div class="my-3">
                                     <x-label for="birthday" :value="__('Birthday')"/>
                                     <x-input name="birthday"
                                             type="date"
                                             class="block  w-full"
                                             value="{{ old('birthday') }}"/>
                                     </div>
                                 </div>

                                 <div class="my-3">
                                    <x-label for="address" :value="__('Address')"/>
                                    <x-input name="address"
                                            type="text"
                                            class="block w-full"
                                            value="{{ old('address') }}"/>
                                </div>

                                <div class="grid grid-cols-2 gap-4">
                                    <div class="my-3">
                                        <x-label for="sports" :value="__('Sports')"/>
                                        <x-input name="sports"
                                                type="text"
                                                class="block w-full"
                                                value="{{ old('sports') }}"/>
                                    </div>
                                    <div class="my-3">
                                        <x-label for="status" :value="__('Status')"/>
                                        <select name="status" class="mt-1 w-full border-gray-300 rounded-md shadow-sm focus:border-primary-300 focus:ring focus:ring-primary-200 focus:ring-opacity-50 focus-within:text-primary-600">
                                            <option value="" selected disabled>-- Select Status --</option>
                                            <option value="Active">Active</option>
                                            <option value="Injured">Injured</option>
                                            <option value="Study Leave">Study Leave</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="my-3">
                                    <x-label for="avatar" :value="__('Picture')"/>
                                    <x-input name="avatar"
                                            type="file"
                                            class="block w-full"
                                            value="{{ old('avatar') }}"/>
                                </div>
                            </div>

                            <div class="mt-4">
                                <x-button class="block ">
                                    {{ __('Create Athlete') }}
                                </x-button>
                            </div>
                        </form>
                </div>
            </div>

        </div>

    </div>
</x-app-layout>
