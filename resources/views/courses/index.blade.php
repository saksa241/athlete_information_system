<x-app-layout>
    <x-slot name="header">
        {{ __('Users') }}
    </x-slot>

    <div class="p-4 bg-white rounded-lg shadow-xs">

        <div class="inline-flex overflow-hidden mb-4 w-full bg-white rounded-lg shadow-md">
            <div class="w-full" x-data="{ show: true }" x-show.transition.opacity.out.duration.2000ms="show" x-init="setTimeout(() => show = false, 5000)">
                <x-success-message class="w-full"/>
            </div>
        </div>

        <div class="mb-5">
            <x-a href="{{ route('course.create') }}">
                {{ __('Add Course') }}
            </x-a>
        </div>

        <div class="overflow-hidden mb-8 w-full rounded-lg border shadow-xs">
            <div class="overflow-x-auto w-full">
                <table class="w-full whitespace-no-wrap">
                    <thead>
                    <tr class="text-xs font-semibold tracking-wide text-left text-gray-500 uppercase bg-gray-50 border-b">
                        <th class="px-4 py-3">Description</th>
                        <th class="px-4 py-3">College</th>
                        <th class="px-4 py-3 ">Action</th>
                    </tr>
                    </thead>
                    <tbody class="bg-white divide-y">
                        @foreach ($courses as $course)
                        <tr>
                            <td class="px-4 py-3 text-sm">
                                {{ $course->description }}
                            </td>

                            <td class="px-4 py-3 text-sm">
                                {{$course->college  }}
                            </td>

                            <td class="px-4 py-3 text-center">
                              <div class="flex">
                                <div class="mr-2">
                                    <a href="{{ route('course.edit', $course) }}">
                                        <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-500 text-white">
                                            Update
                                          </span>
                                       </a>
                                   </div>

                                   <div>
                                    <form action="{{ route('course.destroy', $course) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" onclick="return confirm('Are you sure?')">
                                            <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-red-500 text-white">
                                                Delete
                                              </span>
                                        </button>
                                    </form>
                                   </div>
                              </div>

                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="px-4 py-3 text-xs font-semibold tracking-wide text-gray-500 uppercase bg-gray-50 border-t sm:grid-cols-9">
                    {{ $courses->links() }}
                </div>
        </div>

    </div>
</x-app-layout>
