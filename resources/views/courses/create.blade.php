<x-app-layout>
    <x-slot name="header">
        {{ __('Create Course') }}
    </x-slot>

    <div class="p-4 bg-white rounded-lg shadow-xs">
        <div class="p-5 overflow-hidden w-full rounded-lg border shadow-xs">
            <x-auth-validation-errors />
            <div class="overflow-x-auto w-full">
                <div class="flex items-center justify-center p-6 sm:p-12">
                    <div class="w-full">
                        <form method="POST" action="{{ route('course.store') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="w-full">
                                <div class="grid grid-cols-2 gap-4">
                                    <div class="my-3">
                                         <x-label for="description" :value="__('Decription')"/>
                                         <x-input name="description"
                                                 type="text"
                                                 class="block w-full"
                                                 value="{{ old('description') }}"/>
                                    </div>

                                    <div class="my-3">
                                     <x-label for="college" :value="__('College')"/>
                                     <select name="college" class="mt-1 w-full border-gray-300 rounded-md shadow-sm focus:border-primary-300 focus:ring focus:ring-primary-200 focus:ring-opacity-50 focus-within:text-primary-600">
                                        <option value="" selected disabled>-- Select College --</option>
                                        <option value="CAS">CAS</option>
                                        <option value="COED">COED</option>
                                        <option value="CME">CME</option>
                                    </select>
                                 </div>

                            </div>

                            <div class="mt-4">
                                <x-button class="block ">
                                    {{ __('Create Athlete') }}
                                </x-button>
                            </div>
                        </form>
                </div>
            </div>

        </div>

    </div>
</x-app-layout>
