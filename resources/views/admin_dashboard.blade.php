<x-app-layout>
    <x-slot name="header">
        {{ __('Dashboard') }}
    </x-slot>

    <div class="bg-white rounded-lg shadow-xs">
        <div class="flex w-full ">
            <div class="flex-1 m-5 relative rounded bg-gray-200 shadow">
                <div class="bg-green-500 pl-10 pr-10 pt-8 pb-8 ml-3 absolute top-0 -mt-4 -mr-4 rounded text-white fill-current shadow">
                    <i class="fas fa-running inline-block w-5"></i>
                </div>

                <div class="float-right top-0 right-0 m-3">
                    <div class="text-right text-sm">Athlete</div>
                    <div class="text-right text-3xl">{{ $athletes }}</div>
                </div>
            </div>

            <div class="flex-1 m-5 relative rounded bg-gray-200 shadow">
                <div class="bg-red-500 pl-10 pr-10 pt-8 pb-8 ml-3 absolute top-0 -mt-4 -mr-4 rounded text-white fill-current shadow">
                    <i class="fas fa-envelope inline-block w-5"></i>
                </div>

                <div class="float-right top-0 right-0 m-3">
                    <div class="text-right text-sm">Course</div>
                    <div class="text-right text-3xl">{{ $courses }}</div>
                </div>
            </div>
          </div>

          <div >
            <div class=" m-10 bg-white rounded shadow">
                {!! $chart->container() !!}
            </div>
        </div>
    </div>
    <script src="{{ $chart->cdn() }}"></script>

{{ $chart->script() }}
</x-app-layout>
