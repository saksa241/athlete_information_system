<x-app-layout>
    <x-slot name="header">
        {{ __('Users') }}
    </x-slot>

    <div class="p-4 bg-white rounded-lg shadow-xs">

        <div class="inline-flex overflow-hidden mb-4 w-full bg-white rounded-lg shadow-md">
            <div class="w-full" x-data="{ show: true }" x-show.transition.opacity.out.duration.2000ms="show" x-init="setTimeout(() => show = false, 5000)">
                <x-success-message class="w-full"/>
            </div>
        </div>

        <div class="overflow-hidden mb-8 w-full rounded-lg border shadow-xs">
            <div class="overflow-x-auto w-full">
                <table class="w-full whitespace-no-wrap">
                    <thead>
                    <tr class="text-xs font-semibold tracking-wide text-left text-gray-500 uppercase bg-gray-50 border-b">
                        <th class="px-4 py-3">Name</th>
                        <th class="px-4 py-3">Course</th>
                        <th class="px-4 py-3">Sports</th>
                        <th class="px-4 py-3">Status</th>
                        <th class="px-4 py-3">Birthday</th>
                        <th class="px-4 py-3">Address</th>
                    </tr>
                    </thead>
                    <tbody class="bg-white divide-y">
                        <tr class="text-gray-700">
                            <td class="px-4 py-3 text-sm">
                                <div class="flex items-center">
                                  <div class="flex-shrink-0 h-10 w-10">
                                    @if (auth()->user()->avatar)
                                        <img class="h-10 w-10 rounded-full" src="{{ asset('images/profile/'.auth()->user()->avatar) }}" alt="">
                                    @else
                                        <img class="h-10 w-10 rounded-full" src="https://ui-avatars.com/api/?name={{ auth()->user()->name }}" alt="">
                                    @endif
                                  </div>
                                  <div class="ml-4">
                                    <div class="text-sm font-medium text-gray-900">
                                      {{ auth()->user()->name }}
                                    </div>
                                    <div class="text-sm text-gray-500">
                                      {{ auth()->user()->email }}
                                    </div>
                                  </div>
                                </div>
                              </td>

                            <td class="px-4 py-3 text-sm">
                                {{ auth()->user()->course->description }} (  {{ auth()->user()->year_level }})
                            </td>

                            <td class="px-4 py-3 text-sm">
                                {{ auth()->user()->sports }}
                            </td>

                            <td class="text-center text-sm w-auto">
                                @livewire('status')
                            </td>

                            <td class="px-4 py-3 text-sm">
                                {{ auth()->user()->birthday }} ({{ auth()->user()->age }})
                            </td>

                            <td class="px-4 py-3 text-sm">
                                {{ auth()->user()->address }}
                            </td>
                        </tr>
                    @foreach($users as $user)
                        @if (!$user->isAn('admin') && $user->id !== auth()->user()->id)
                        <tr class="text-gray-700">
                            <td class="px-4 py-3 text-sm">
                                <div class="flex items-center">
                                  <div class="flex-shrink-0 h-10 w-10">
                                    @if ($user->avatar)
                                        <img class="h-10 w-10 rounded-full" src="{{ asset('images/profile/'.$user->avatar) }}" alt="">
                                    @else
                                        <img class="h-10 w-10 rounded-full" src="https://ui-avatars.com/api/?name={{ $user->name }}" alt="">
                                    @endif
                                  </div>
                                  <div class="ml-4">
                                    <div class="text-sm font-medium text-gray-900">
                                      {{ $user->name }}
                                    </div>
                                    <div class="text-sm text-gray-500">
                                      {{ $user->email }}
                                    </div>
                                  </div>
                                </div>
                              </td>

                            <td class="px-4 py-3 text-sm">
                                {{ $user->course->description }} (  {{ $user->year_level }})
                            </td>

                            <td class="px-4 py-3 text-sm">
                                {{ $user->sports }}
                            </td>

                            <td class="text-center text-sm w-auto">
                                @if ($user->status == 'Active')
                                    <span class="px-2 w-auto inline-flex text-xs leading-5 font-semibold rounded bg-green-500 text-white">
                                        {{ $user->status }}
                                    </span>
                                @endif

                                @if ($user->status == 'Study Leave')
                                    <span class="px-2 w-auto inline-flex text-xs leading-5 font-semibold rounded bg-yellow-500 text-white">
                                        {{ $user->status }}
                                    </span>
                                @endif

                                @if ($user->status == 'Injured')
                                    <span class="px-2 w-auto inline-flex text-xs leading-5 font-semibold rounded bg-red-500 text-white">
                                        {{ $user->status }}
                                    </span>
                                @endif

                            </td>

                            <td class="px-4 py-3 text-sm">
                                {{ $user->birthday }} ({{ $user->age }})
                            </td>

                            <td class="px-4 py-3 text-sm">
                                {{ $user->address }}
                            </td>
                        </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="px-4 py-3 text-xs font-semibold tracking-wide text-gray-500 uppercase bg-gray-50 border-t sm:grid-cols-9">
                {{ $users->links() }}
            </div>
        </div>
    </div>

</x-app-layout>
