<?php

namespace Database\Seeders;

use App\Models\Course;
use Illuminate\Database\Seeder;

class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $courses = [
            [
                'description' => 'ABCOM',
                'college' => 'CAS'
            ],
            [
                'description' => 'AB POL SCI',
                'college' => 'CAS'
            ],
            [
                'description' => 'AB ENGLISH',
                'college' => 'CAS'
            ],
            [
                'description' => 'BEED',
                'college' => 'COED'
            ],

            [
                'description' => 'BSED',
                'college' => 'COED'

            ],
            [
                'description' => 'BSHAE',
                'college' => 'CME'

            ],
            [
                'description' =>   'BSHRM',
                'college' => 'CME'

            ],
            [
                'description' =>  'BSSW',
                'college' => 'CAS'

            ],
            [
                'description' =>   'BSTHRM',
                'college' => 'CME'

            ],

            [
                'description' =>   'BSIT',
                'college' => 'CAS'

            ],

        ];

        foreach ($courses as $course) {
            Course::create($course);
        }
    }
}
