<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->isAn('admin')) return redirect()->route('admin.dashboard.index');

        if (auth()->user()->isAn('athlete')) return redirect()->route('athlete.dashboard.index');

        // if (auth()->user()) return view('auth.login');
    }
}
