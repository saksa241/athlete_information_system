<?php

namespace App\Http\Controllers;

use App\Http\Requests\AthleteRequest;
use App\Models\Course;
use App\Models\User;
use Illuminate\Support\Facades\File;

class UserController extends Controller
{
    public function index()
    {
        $users = User::with('course')->paginate(10);

        return view('users.index', compact('users'));
    }

    public function create()
    {
        $courses = Course::all();
        return view('users.create', compact('courses'));
    }

    public function store(AthleteRequest $request)
    {

        $athlete = User::create($request->validated());
        $athlete->attachRole('athlete');

        if ($request->hasFile('avatar')) {
            $avatar =  $athlete->id . '-' . $request->email . '.'  . $request->avatar->getClientOriginalExtension();
            $request->avatar->move(public_path('images/profile'), $avatar);
            $athlete->update(['avatar' => $avatar]);
        }
        return redirect()->route('users.index')->with('message', 'Athlete Added Successful');
    }

    public function edit(User $user)
    {
        $courses = Course::all();
        return view('users.edit', compact('user', 'courses'));
    }

    public function update(AthleteRequest $request, User $user)
    {

        if ($request->hasFile('avatar') && $user->avatar)  unlink("images/profile/" . $user->avatar);

        $user->update($request->validated());

        if ($request->hasFile('avatar')) {
            $avatar =  $user->id . '-' . $request->email . '.'  . $request->avatar->getClientOriginalExtension();
            $request->avatar->move(public_path('images/profile'), $avatar);
            $user->update(['avatar' => $avatar]);
        }

        return redirect()->route('users.index')->with('message', 'Athlete Updated Successful');
    }

    public function destroy(User $user)
    {
        $user->delete();
        File::delete('images/profile/' . $user->avatar);
        return redirect()->route('users.index')->with('message', 'Athlete Susccessfully Delete');
    }
}
