<?php

namespace App\Http\Controllers;

use App\Charts\AtheleteStatusChart;
use App\Models\Course;
use App\Models\User;
use Illuminate\Http\Request;

class AdminDashboardController extends Controller
{
    public function index(AtheleteStatusChart $chart)
    {
        $athletes = User::whereRoleIs('athlete')->count();
        $courses = Course::count();
        return view('admin_dashboard', compact('athletes', 'courses'))
            ->with(['chart' => $chart->build()]);
    }
}
