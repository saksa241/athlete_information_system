<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Component;
use Livewire\WithPagination;

class Status extends Component
{
    use WithPagination;
    public $status = null;

    public function render()
    {
        return view('livewire.status');
    }

    public function mount()
    {
        $this->status = auth()->user()->status;
    }

    public function updatedStatus($value)
    {

        auth()->user()->update([
            'status' => $value
        ]);

        return redirect()->route('athlete.dashboard.index')->with('message', 'Status Updated Succesfull');
    }
}
