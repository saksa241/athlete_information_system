<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AthleteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => ['required', 'email',  Rule::unique('users')->ignore($this->user)],
            'course_id' => 'required',
            'year_level' => 'required',
            'age' => 'required',
            'birthday' => 'required|date',
            'address' => 'required',
            'sports' => 'required',
            'status' => 'required',
            'avatar' => 'image|mimes:jpeg,png,jpg,gif|max:5048|nullable'
        ];
    }
}
