<?php

namespace App\Charts;

use App\Models\User;
use ArielMejiaDev\LarapexCharts\LarapexChart;

class AtheleteStatusChart
{
    protected $chart;

    public function __construct(LarapexChart $chart)
    {
        $this->chart = $chart;
    }

    public function build(): \ArielMejiaDev\LarapexCharts\PieChart
    {
        $active = User::whereRoleIs('athlete')
            ->where('status', 'active')
            ->count();
        $injured = User::whereRoleIs('athlete')
            ->where('status', 'injured')
            ->count();
        $sl = User::whereRoleIs('athlete')
            ->where('status', 'Study Leave')
            ->count();

        return $this->chart->pieChart()
            ->setTitle('Athlete Status')
            ->addData([$active, $injured, $sl])
            ->setColors(['#0E9F6E', '#F05252', '#C27803'])
            ->setHeight(400)
            ->setLabels(['Active', 'Injured', 'Study Leave']);
    }
}
